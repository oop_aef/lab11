package com.anusit.week11;

public interface Walkable {
    public void walk();
    public void run();
}
