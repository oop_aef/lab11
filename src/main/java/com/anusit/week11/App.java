package com.anusit.week11;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Bird bird1 = new Bird("Tweety");
        bird1.eat();
        bird1.sleep();
        bird1.takeoff();
        bird1.fly();
        bird1.landing();
        Plane boeing = new Plane("Boeing", "Rosaroi");
        boeing.takeoff();
        boeing.fly();
        boeing.landing();
        Superman clark = new Superman("clark");
        clark.takeoff();
        clark.fly();
        clark.landing();
        clark.walk();
        clark.run();
        clark.eat();
        clark.sleep();
        clark.swim();
        Human man1 = new Human("Man");
        man1.eat();
        man1.sleep();
        man1.walk();
        man1.run();
        man1.swim();
        Bat bat1 = new Bat("bat1");
        bat1.takeoff();
        bat1.fly();
        bat1.landing();
        bat1.eat();
        bat1.sleep();
        Snake snake1 = new Snake("snake1");
        snake1.eat();
        snake1.sleep();
        snake1.crawl();
        Rat rat1 = new Rat("rat1");
        rat1.walk();
        rat1.run();
        rat1.eat();
        rat1.sleep();
        rat1.swim();
        rat1.crawl();
        Dog dog1 = new Dog("dog1");
        dog1.walk();
        dog1.run();
        dog1.eat();
        dog1.sleep();
        dog1.swim();
        dog1.crawl();
        Cat cat1 = new Cat("cat1");
        cat1.walk();
        cat1.run();
        cat1.eat();
        cat1.sleep();
        cat1.swim();
        cat1.crawl();
        Fish fish1 = new Fish("fish1");
        fish1.eat();
        fish1.sleep();
        fish1.swim();
        Crocodile crocodile1 = new Crocodile("crocodile1");
        crocodile1.walk();
        crocodile1.run();
        crocodile1.eat();
        crocodile1.sleep();
        crocodile1.swim();
        crocodile1.crawl();
        Submarine submarine1 = new Submarine("submarine1", "Diesel");
        submarine1.swim();
        System.out.println();


        System.out.println("**Flyable**");
        Flyable[] flyables = {bird1, boeing, clark, bat1};
        for(int i=0; i<flyables.length; i++) {
            flyables[i].takeoff();
            flyables[i].fly();
            flyables[i].landing();
        }
        System.out.println();

        System.out.println("**Walkable**");
        Walkable[] walkables = {bird1, clark, man1, rat1, dog1, cat1, crocodile1};
        for(int i=0; i<walkables.length; i++) {
            walkables[i].walk();
            walkables[i].run();
        }
        System.out.println();

        System.out.println("**Swimable**");
        Swimable[] swimables = {clark, man1, rat1, dog1, cat1, fish1, crocodile1, submarine1};
        for (int i = 0; i < swimables.length; i++) {
            swimables[i].swim();
        }
        System.out.println();
        
        System.out.println("**Crawlable**");
        Crawlable[] crawlables = {snake1, rat1, dog1, cat1, crocodile1};
        for (int i = 0; i < crawlables.length; i++) {
            crawlables[i].crawl();
        }
    }
}
